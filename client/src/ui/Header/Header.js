import styled from 'styled-components'

const HeaderContainer = styled.div`
  display: flex;
  align-items: center;
  height: 50px;
  background-color: white;
  padding: 0 10px;
  position: fixed;
  width: 100%;
  box-sizing: border-box;
  z-index: 3;
  box-shadow: 0 0 3px #bbb;
`;

const HeaderItem = styled.div`
  font-size: 16px;
  display: flex;
  color: ${props => props.theme.textMainColor};
  cursor: pointer;
  margin-right: 35px;

  &:hover {
    color: ${props => props.theme.mainGreen};
  }

  &:active {
    color: ${props => props.theme.mainGreen};
  }

  &.active {
    text-transform: capitalize;
  }
`;

const HeaderLogo = styled.img`
  height: 36px;
  cursor: pointer;
  display: block;
`;

const RightContent = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
  height: 100%;
`;

const MenuIconBlock = styled.img`
  width: 30px;
  cursor: pointer;
  display: block;
`;

export {
  HeaderContainer,
  HeaderItem,
  HeaderLogo,
  RightContent,
  MenuIconBlock
}
