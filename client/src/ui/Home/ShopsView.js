import styled from 'styled-components'

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  flex-shrink: 0;

  @media screen and (max-width: 768px) {
    margin-bottom: 10px;
  }
`;

const CitiesContainer = styled.div`
  height: 40px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
`;

const City = styled.div`
  width: 2.5em;
  height: 100%;
  cursor: pointer;
  display: flex;
  font-size: 24px;
  font-weight: bold;
  align-items: center;
  justify-content: center;
  
  &.selected {
    color: ${props => props.theme.mainGreen};
  }
`;

const ViewSelector = styled.div`
  border: 1px solid #3c3c3c;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-bottom: 20px;
`

const View = styled.div`
  width: 30px;
  height: 30px;
  cursor: pointer;
  display: flex;
  font-size: 18px;
  padding: 5px;
  font-weight: bold;
  align-items: center;
  justify-content: center;

  &.selected {
    background-color: ${props => props.theme.mainGreen};
  }
`;

export {
  ColumnContainer,
  City,
  CitiesContainer,
  ViewSelector,
  View
}
