import styled from 'styled-components'
import AndyWarhol from '../../components/Home/img/andy_warhol.png'

const LeadContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-image: url(${AndyWarhol});
  background-position: center center;
  background-size: cover;
  height: 200px;
`

const Title = styled.div`
  color: white;
  font-family: Georgia, serif;
  font-style: italic;
  font-size: 18px;
  padding: 5px 10px;
  text-align: center;
  background-color: black;
`

const LeadButtons = styled.div`
  margin: 20px 0;
  width: 300px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  color: white;
`;

const LeadButton = styled.div`
  width: 130px;
  padding: 10px 0;
  text-align: center;
  border: 2px solid white;
  color: white;
  background-color: rgba(143, 146, 152, 0.65);
  font-size: 20px;
`

export {
  LeadContainer,
  Title,
  LeadButtons,
  LeadButton
}
