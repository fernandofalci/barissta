import styled from 'styled-components'

const ShopsListContainer = styled.div`
  width: 100%;
  height: 350px;
  display: flex;
  flex-direction: column;
  padding-bottom: 66px;

  @media screen and (min-width: 768px) {
    height: 400px;
  }
`;

const List = styled.div`
  flex: 1 1 auto;
  overflow: auto;
  min-height: 0px;
  height: 100%;
  -webkit-overflow-scrolling: touch;
  &::-webkit-scrollbar {
    display: none;
  }
`

const Shop = styled.div`
  background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.2)), url(${props => props.imageUrl});
  height: 95px;
  background-position: center center;
  background-size: cover;
  color: white;
  cursor: pointer;
  text-shadow: 0 1px 1px black;
  font-weight: bold;
  border-bottom: 0.5px solid white;
  border-top: 0.5px solid white;
`;

const ShopDescription = styled.div`
  padding: 15px;
`

const ShopAddress = styled.div`
  display: flex;
  align-items: center;
`

const ShopAddressText = styled.div`
  font-family: 'Source Sans Pro', sans-serif;
  font-Size: 12px;
  margin-left: 5px;
`

const ShopName = styled.div`
  font-size: 22px;
  margin-bottom: 3px;
`

export {
  ShopsListContainer,
  Shop,
  ShopDescription,
  List,
  ShopAddress,
  ShopAddressText,
  ShopName
}
