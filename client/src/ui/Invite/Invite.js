import styled from 'styled-components';

const InviteContainer = styled.div`
  min-height: 448px;
  width: 100%;
  flex: 0 1 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Text = styled.div`
  margin-top: 15px;
  font-size: 26px;
`;

const CodeContainer = styled.div`
  width: 230px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid ${props => props.theme.mainGreen};
  border-radius: 2px;
  margin: 15px;
  font-size: 20px;
  color: gray;
`;

const InviteDescription = styled.div`
  width: 230px;
  margin-bottom: 20px;
  text-align: center;
  font-family: 'Source Sans Pro', sans-serif;
`;

const ShareIcons = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  width: 200px;
  align-items: center;
`;

const Icon = styled.i`
  cursor: pointer;

  &.fa-facebook-square {
    color: #4267b2;
    font-size: 43px;
  }

  &.fa-facebook-square:hover {
    color: #000000;
  }

  &.fa-whatsapp {
    color: #ffffff;
    font-size: 32px;
  }
`;

const WhatsappIcon = styled.div`
  display: flex;
  cursor: pointer;
  width: 38px;
  height: 38px;
  border-radius: 6px;
  align-items: center;
  justify-content: center;
  background-color: #00e676;

  &:hover {
    background-color: black;
  }
`;

const EarnedCupsContainer = styled.div`
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid ${props => props.theme.mainGreen};
  border-radius: 2px;
  font-size: 30px;
  margin: 15px;
`;

export { InviteContainer, CodeContainer, InviteDescription, Text, EarnedCupsContainer, ShareIcons, Icon, WhatsappIcon }
