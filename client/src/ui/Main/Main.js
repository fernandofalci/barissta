import styled from 'styled-components';

const Main = styled.div`
  width: 100%;
  height: calc(100% - 50px);
  position: absolute;
  top: 50px;
  display: flex;
`;

const SidebarShadow = styled.div`
  z-index: 1;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  opacity: 0;
  visibility: hidden;
  transition: opacity 0.3s ease-out;
  background-color: rgba(0, 0, 0, 0.4);

  &.sidebarOpen {
    opacity: 1;
    visibility: visible;
  }
`

const CookieWarningBlock = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 55px;
  z-index: 5;
  background-color: rgba(0, 0, 0, 0.8);
  bottom: 0px;
  color: white;
`

const CookieWarningButton = styled.div`
  padding: 0 5px;
  height: 20px;
  border-radius: 2px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  background-color: ${props => props.alt ? props.theme.textSecondaryColor : props.theme.mainGreen};
  color: ${props => props.alt ? 'black' : props.theme.textSecondaryColor};
  flex: 0 0 auto;
  font-size: 10px;
  margin-right: 10px;

  @media screen and (min-width: 769px) {
    font-size: 12px;
  }
`

const CookieWarningText = styled.div`
  font-size: 11px;
  display: inline-block;
  margin: 0 10px;

  @media screen and (min-width: 769px) {
    font-size: 16px;
    margin-left: 0;
  }
`

export {
  Main,
  SidebarShadow,
  CookieWarningBlock,
  CookieWarningButton,
  CookieWarningText
}
