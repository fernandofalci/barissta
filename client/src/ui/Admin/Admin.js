import styled from 'styled-components';

const AdminContainer = styled.div`
  flex: 1 0 auto;
  min-height: 400px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const UserContainer = styled.div`
  flex: 0 0 20px;
  height: 20px;
  width: 320px;
  font-size: 10px;
  margin-bottom: 10px;
  background-color: #B1E6CD;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const ShopContainer = styled.div`
  flex: 0 0 20px;
  width: 320px;
  font-size: 10px;
  margin-bottom: 10px;
  background-color: #F1BAC1;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const SummaryContainer = styled.div`
  flex: 0 0 20px;
  width: 320px;
  height: 30px;
  font-size: 20px;
  margin-bottom: 10px;
  background-color: lightblue;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
`;

export { AdminContainer, UserContainer, ShopContainer, SummaryContainer };
