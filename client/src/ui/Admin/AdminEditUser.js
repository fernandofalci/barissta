import styled from 'styled-components';

const Button = styled.div`
  font-size: 12px;
  background-color: black;
  color: white;
  border-radius: 2px;
  font-family: 'Source Sans Pro', sans-serif;
  padding: 3px;
`;

const CancelButton = styled.div`
  font-size: 12px;
  background-color: red;
  color: white;
  border-radius: 2px;
  font-family: 'Source Sans Pro', sans-serif;
  padding: 3px;
`;

const UserCurrentCredit = styled.input`
  font-size: 12px;
  width: 50px;
`;

const EditUserContainer = styled.div`
  height: 20px;
  width: 130px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export { EditUserContainer, Button, CancelButton, UserCurrentCredit };
