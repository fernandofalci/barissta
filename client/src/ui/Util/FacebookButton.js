import styled from 'styled-components'

const ButtonContainer = styled.div`
  background-color: #4267B2;
  color: white;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  font-family: 'Source Sans Pro', sans-serif;
  height: 40px;
  width: 100%;
`;

const Logo = styled.i`
  margin-left: 15px;
  margin-right: 30px;
  font-size: 20px;
`

export {
  ButtonContainer,
  Logo
}
