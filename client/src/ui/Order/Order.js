import styled from 'styled-components'

const OrderScreen = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: ${props => props.fixed ? 'fixed;' : 'static;' };
`

const OrderBlock = styled.div`
  width: 100%;
  display: flex;
  flex: 1 0 auto;
  flex-direction: column;
  align-items: center;
  padding-bottom: 120px;

  &.locked {
    padding-top: 200px;
  }

  &.checkout {
    padding-bottom: 0;
  }
`

const ItemsContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`

const OrderTitle = styled.div`
  margin: 20px 0 10px 0;
  text-align: center;
  font-size: 20px;
  width: 250px;
  color: black;
  font-weight: bold;
  border-bottom: 1px solid black;
  padding: 5px 0;
`

const OrderButtonStyle = styled.div`
  height: 50px;
  width: 94%;
  display: flex;
  flex: 0 0 auto;
  align-items: center;
  justify-content: center;
  color: white;
  background-color: ${props => props.theme.mainGreen};
  font-size: 20px;
  font-weight: bold;
  margin: 0 auto;
  margin-bottom: 10px;
  font-family: 'Source Sans Pro', sans-serif;
  transition: 0.3s all ease;

  &.disabled {
    border: 1px solid #999999;
    background-color: #cccccc;
    color: #666666;
  }

  &.active {
    background-color: black;
    transition: 0.3s all ease;
  }
`

const OrderDetails = styled.div`
  width: 100%;
  background-color: white;
  height: 124px;
  position: fixed;
  bottom: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  box-shadow: 0 0 9px #bbb;
`

export {
  OrderScreen,
  OrderBlock,
  ItemsContainer,
  OrderTitle,
  OrderButtonStyle,
  OrderDetails
}
