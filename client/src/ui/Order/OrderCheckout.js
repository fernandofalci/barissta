import styled from 'styled-components';

const BackButton = styled.div`
  width: 200px;
  height: 40px;
  border-radius: 2px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border: 1px solid ${props => props.theme.mainGreen};
  background-color: ${props => props.theme.textSecondaryColor};
  color: ${props => props.theme.mainGreen};
  flex: 0 0 auto;
  font-size: 18px;
  margin: 10px 0;
  margin-bottom: 140px;
  font-family: 'Source Sans Pro', sans-serif;
`

const EmptyCart = styled.div`
  font-family: 'Source Sans Pro', sans-serif;
  margin-top: 20px;
  margin-bottom: 10px;
  font-size: 20px;
`

const CheckoutTitle = styled.div`
  font-size: 20px;
  background-color: black;
  color: white;
  font-weight: bold;
  padding: 5px 10px;
  margin-left: 16px;
  margin-bottom: 5px;
  clear: both;
  float: left;
`

const CheckoutTitles = styled.div`
  margin: 20px 0 10px 0;
  height: 70px;
  width: 100%;
  display: inline-block;
`

export {
  BackButton,
  EmptyCart,
  CheckoutTitle,
  CheckoutTitles
}
