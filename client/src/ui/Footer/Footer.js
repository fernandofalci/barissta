import styled from 'styled-components'

const FooterContainer = styled.div`
  width: 100vw;
  flex: 0 0 auto;
  flex-direction: column;
  background-color: ${props => props.theme.mainGreen};
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 20px;
  padding-bottom: 40px;
`;

const Logo = styled.img`
  height: 40px;
`;

const LogoContainer = styled.div`
  margin: 20px 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
  height: 45px;
`;

const FooterCoffeeShopsContainer = styled.div`
  font-family: 'Source Sans Pro', sans-serif;
`;

const SocialMedia = styled.div`
  width: 50px;
  display: flex;
  justify-content: space-between;
`;

const FooterCoffeeShop = styled.div`
  font-size: 11px;
  margin-bottom: 10px;
  color: #3c3c3c;
  width: 50%;

  @media screen and (min-width: 426px) {
    font-size: 14px;
  }
`;

const FooterShops = styled.div`
  width: 270px;
  font-size: 14px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;

  @media screen and (min-width: 769px) {
    width: 500px;
  }
`;

const FooterCoffeeShopTitle = styled.div`
  font-weight: bold;
  text-align: center;
  margin: 20px 0;
`;


export { FooterContainer, Logo, FooterShops, FooterCoffeeShopTitle,
         LogoContainer, FooterCoffeeShopsContainer, SocialMedia, FooterCoffeeShop };