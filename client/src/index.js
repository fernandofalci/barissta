import _ from 'lodash'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import { saveState, loadState } from './utils/localStorage'

// components
import App from './App';

// styles
import './index.css';

// redux store
import configureStore from './redux/create'

const persistedState = loadState()
const store = configureStore(persistedState);
store.subscribe(_.throttle(() => {
  saveState({
   cart: store.getState().cart
  })
}, 1000))

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
