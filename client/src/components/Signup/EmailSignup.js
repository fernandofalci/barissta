import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { 
  LoginScreen, InputsContainer,
  SignupBox, Button, Input, Loading, FlashMessage, Text
} from './../../ui/Login/Login'
import { SignupContainer } from './../../ui/Signup/Signup'
import { Highlighted } from './../../ui/Home/Home'

class EmailSignup extends React.PureComponent {
  static propTypes = {
    onSignup: PropTypes.func.isRequired,
    signupState: PropTypes.object.isRequired
  }

  state = {
    name: '',
    email: '',
    password: '',
    errorMessage: ''
  }

  onInputKeyUp = (event) => {
    if (event.key === 'Enter') {
      this.onSubmitSignup();
    }
  }

  onNameChange = (event) => {
    this.setState({ name: event.target.value });
  }

  onEmailChange = (event) => {
    this.setState({ email: event.target.value });
  }

  onPasswordChange = (event) => {
    this.setState({ password: event.target.value });
  }
  
  onSubmitSignup = () => {
    const { name, email, password } = this.state;
    const { onSignup } = this.props;
    onSignup({ name, email, password });
  }

  componentWillReceiveProps(nextProps) {
    const { signupState } = nextProps;
    this.setState({ errorMessage: signupState.errorMessage });
    if (signupState.errorMessage) {
      window.scrollTo(0, 0)
    }
  }

  renderLoading = () => {
    const { signupState } = this.props
    if (signupState.isSigning) {
      return (
        <Loading><i className="fa fa-spinner fa-spin" aria-hidden="true" /></Loading>
      )
    }
  }

  render() {
    const { name, email, password, errorMessage } = this.state;
    return (
      <LoginScreen>
        { errorMessage && <FlashMessage>{ errorMessage }</FlashMessage> }
        <SignupContainer>
          { this.renderLoading() }
          <Text>REGÍSTRATE EN <Highlighted>BARISSTA</Highlighted></Text>
          <div style={{marginTop: '70px', marginBottom: '50px', width: '100%'}}>
            <InputsContainer>
              <Input tabindex="1" type='text' placeholder='Nombre' value={name} onChange={this.onNameChange} onKeyUp={this.onInputKeyUp} />
              <Input tabindex="2" type='email' placeholder='Correo electrónico' value={email} onChange={this.onEmailChange} onKeyUp={this.onInputKeyUp} />
              <Input tabindex="3" type='password' placeholder='Contraseña' value={password} onChange={this.onPasswordChange} onKeyUp={this.onInputKeyUp} />
            </InputsContainer>
          </div>
          <Button onClick={this.onSubmitSignup}>Entrar</Button>
        </SignupContainer>
        <Link to="/login" style={{width: '100%'}}>
          <SignupBox>
            <div>¿Ya tienes cuenta? Inicia sesión</div>
          </SignupBox>
        </Link>
      </LoginScreen>
    );
  }
}

export default EmailSignup
