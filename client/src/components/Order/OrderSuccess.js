import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { LocationCheckScreen, LocationCheckTitle, LocationCheckDescription,
         LocationCheckButton, LocationCheckContent } from './../../ui/Order/OrderLocationCheck'

class OrderSuccess extends React.PureComponent {
  static propTypes = {
    onClickOk: PropTypes.func.isRequired
  }

  render() {
    const { onClickOk } = this.props

    return (
        <LocationCheckScreen>
          <LocationCheckContent>
            <LocationCheckTitle>¡Disfruta de tu pedido!</LocationCheckTitle>
            <LocationCheckDescription>Ya hemos añadido el crédito de esta consumición a tu cuenta</LocationCheckDescription>
            <LocationCheckDescription>Todos los meses recibirás una recompensa por tu fidelidad</LocationCheckDescription>
            <LocationCheckButton onClick={onClickOk}>¡ALRIGHT!</LocationCheckButton>
          </LocationCheckContent>
        </LocationCheckScreen>
    )
  }
}

export default OrderSuccess
