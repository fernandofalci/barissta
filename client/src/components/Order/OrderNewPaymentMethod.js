import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import { withRouter } from 'react-router'

import OrderSummary from './../../components/Order/OrderSummary'
import OrderButton from './../../components/Order/OrderButton'
import { OrderScreen, OrderTitle } from './../../ui/Order/Order'
import { PaymentMethodForm, PaymentMethodField, 
         PaymentMethodBlock, CardErrors } from './../../ui/Order/OrderNewPaymentMethod'
import { Loading } from './../../ui/Login/Login'

import { stripeKey } from '../../utils/config'
import { parseURLQuery } from '../../utils/helper'

class OrderNewPaymentMethod extends React.PureComponent {
  constructor() {
    super()
    this.stripe = window.Stripe(stripeKey)
    this.cardNumberElement = null
  }

  static propTypes = {
    cart: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    onAddPaymentMethod: PropTypes.func.isRequired,
    addPaymentMethodState: PropTypes.object.isRequired
  }

  componentDidMount() {
    const elements = this.stripe.elements()

    const style = {
      base: {
        fontSize: '16px',
        lineHeight: '28px',
      },
      complete: {
        color: '#10ABA3'
      }
    }

    const cardNumberElement = elements.create('cardNumber', { style })
    const cardExpiryElement = elements.create('cardExpiry', { style })
    const cardCvcElement = elements.create('cardCvc', { style })

    cardNumberElement.mount('#card-number-element')
    cardExpiryElement.mount('#card-expiry-element')
    cardCvcElement.mount('#card-cvc-element')

    cardNumberElement.addEventListener('change', ({ error }) => {
      const displayError = document.getElementById('card-errors')
      if (error) {
        displayError.textContent = error.message
      } else {
        displayError.textContent = ''
      }
    })

    this.cardNumberElement = cardNumberElement
  }

  componentWillReceiveProps(nextProps) {
    const { history, addPaymentMethodState } = this.props
    const { addPaymentMethodState: nextAddPaymentMethodState } = nextProps
    const didSucceed = addPaymentMethodState.isPending && nextAddPaymentMethodState.didSucceed
    if (didSucceed) {
      const shopParam = parseURLQuery(this.props.location.search).shop
      const checkoutUrl = `/order/checkout?shop=${shopParam}`
      return history.push(checkoutUrl)
    }
  }

  renderLoading = () => {
    const { addPaymentMethodState } = this.props
    if (addPaymentMethodState.isPending) {
      return (
        <Loading><i className="fa fa-spinner fa-spin" aria-hidden="true" /></Loading>
      )
    }
  }

  stripeTokenHandler = async token => {
    const { onAddPaymentMethod } = this.props
    onAddPaymentMethod({ stripeTokenId: token.id })
  }

  isOrderButtonDisabled = () => {
    const { addPaymentMethodState } = this.props
    return addPaymentMethodState.isPending
  }

  onClickCheckoutButton = async () => {
    const { token, error } = await this.stripe.createToken(this.cardNumberElement);
    if (error) {
      const errorElement = document.getElementById('card-errors');
      errorElement.textContent = error.message;
    } else {
      this.stripeTokenHandler(token);
    }
  }

  render() {
    const { cart } = this.props
    return (
      <OrderScreen>
        <div style={{flex: '1 0 auto'}}>
          <OrderTitle>Datos de pago</OrderTitle>
          { this.renderLoading() }
          <div style={{margin: '60px 0'}}>
            <PaymentMethodForm method="post" id="payment-form">
              <PaymentMethodBlock>
                <div style={{width: '100%'}}>
                  <span>Número tarjeta</span>
                  <PaymentMethodField id="card-number-element" />
                </div>
              </PaymentMethodBlock>
              <PaymentMethodBlock>
                <div>
                  <span>Fecha expiración</span>
                  <PaymentMethodField id="card-expiry-element" />
                </div>
                <div style={{width: '25%'}}>
                  <span>CVV</span>
                  <PaymentMethodField id="card-cvc-element" />
                </div>
              </PaymentMethodBlock>
            </PaymentMethodForm>
            <CardErrors id="card-errors" role="alert" />
          </div>
        </div>
        <OrderSummary cart={cart} />
        <OrderButton
          onClickButton={this.onClickCheckoutButton}
          isDisabled={this.isOrderButtonDisabled()}
          content={"Añadir método de pago"}
        />
      </OrderScreen>
    )
  }
}

export default withRouter(OrderNewPaymentMethod)
