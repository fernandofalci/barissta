import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'

import { MapSectionContainer, ColumnContainer, MapContainer } from './../../ui/MapSection/MapSection'

import { googleMapsAPIKey } from '../../utils/config';

class MapSection extends React.PureComponent {
  constructor() {
    super();
    this.map = {}
    this.centerCoords = {
      madrid: { lat: 40.416913, lng: -3.703515 },
      valencia: { lat: 39.471261, lng: -0.371687 }
    }
  }

  static propTypes = {
    shops: PropTypes.array.isRequired,
    city: PropTypes.string.isRequired
  }

  state = {
    openTooltip: {}
  }

  initMap = () => {
    const { city, shops } = this.props
    const map = new window.google.maps.Map(document.getElementById('map'), {
      center: this.centerCoords[city],
      clickableIcons: false,
      scrollwheel: false,
      zoom: 12,
      gestureHandling: 'cooperative'
    })

    this.map = map
    const event = new Event('mapLoaded')
    document.dispatchEvent(event)

    map.addListener('click', () => {
      const { openTooltip } = this.state
      if (!_.isEmpty(openTooltip)) {
        openTooltip.close()
      }
      this.setState({ openTooltip: {} })
    })

    this.renderMarkers(shops)
  }

  asyncLoadMap = () => {
    window.initMap = this.initMap;

    const script = document.createElement("script");
    script.src = `https://maps.googleapis.com/maps/api/js?key=${googleMapsAPIKey}&callback=initMap`
    script.async = true
    script.defer = true

    document.body.appendChild(script)
  }

  setCityCenter = (city) => {
    const selectedCityCoords = this.centerCoords[city]
    this.map.setCenter(selectedCityCoords)
  }

  renderMarkers = (shops) => {
    shops.forEach((shop) => {
      const { lat, lng } = shop.coords;
      const position = new window.google.maps.LatLng(parseFloat(lat), parseFloat(lng))

      const marker = new window.google.maps.Marker({
        position,
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
        map: this.map
      })

      const addressDisplay = shop.address.split(',')[0]
      const weekDay = new Date().getUTCDay()
      const openHours = shop.openHours[weekDay] || 'Cerrado'

      const tooltipStyles = "overflow:hidden;"
      const shopNameStyles = "font-weight:bold;text-align:center;font-size:16px;margin-bottom:10px;"
      const shopInfoStyles = "font-size:10px;width:150px;margin:5px;"
      const shopDetailStyles = "margin-top:3px;"
      const buttonStyles = "display:flex;justify-content:center;align-items:center;border-radius:2px;background-color:#10ABA3;color:white;width:60px;height:28px;font-size:15px;font-weight:bold;"
      const infoWindow = new window.google.maps.InfoWindow({
        content:
          `<div style=${tooltipStyles}>
            <div style=${shopNameStyles}>
              ${shop.name}
            </div>
            <div style="display:flex;align-items:center;justify-content:center;">
              <div>
                <div style=${shopInfoStyles}>
                  <div style="font-weight:bold;">Dirección</div>
                  <div style=${shopDetailStyles}>${addressDisplay}</div>
                </div>
                <div style=${shopInfoStyles}>
                  <div style="font-weight:bold;">Horario</div>
                  <div style=${shopDetailStyles}>${openHours}</div>
                </div>
              </div>
              <a href="/coffee-shop/${shop.slug}">
                <div style=${buttonStyles}>IR</div>
              </a>
            </div>
          </div>`
      })

      marker.addListener('click', () => {
        const { openTooltip } = this.state
        if (!_.isEmpty(openTooltip)) {
          openTooltip.close()
        }
        infoWindow.open(this.map, marker)
        this.setState({ openTooltip: infoWindow })
      })
    })
  }

  componentDidMount() {
    const { shops } = this.props
    if (window.google && window.google.maps) {
      this.initMap()
      this.renderMarkers(shops)
    } else {
      this.asyncLoadMap()
    }
  }

  componentWillReceiveProps(nextProps) {
    const { city, shops } = nextProps
    if (!_.isEmpty(this.map)) {
      this.setCityCenter(city)
      this.renderMarkers(shops)
    } else {
      const mapLoadedListener = () => {
        document.removeEventListener('mapLoaded', mapLoadedListener, false)
        return this.renderMarkers(shops)
      }
      document.addEventListener('mapLoaded', mapLoadedListener, false)
    }
  }

  render() {
    return (
      <MapSectionContainer>
        <ColumnContainer>
          <MapContainer id='map'></MapContainer>
        </ColumnContainer>
      </MapSectionContainer>
    )
  }
}

export default MapSection
