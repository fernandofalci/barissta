import React from 'react'

import { Text, Highlighted } from './../../ui/Home/Home'

export default function BarisstaDescription() {
  return (
    <div>
      <Text>
        ¡Olvídate del efectivo!<br/>Realiza tu pedido con Barissta 
        en tu cafetería favorita, paga a través del móvil y <Highlighted>obtén crédito</Highlighted> con cada consumición.
      </Text>
    </div>
  )
}