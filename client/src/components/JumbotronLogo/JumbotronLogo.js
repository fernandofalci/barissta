import React from 'react';
import { Link } from 'react-router-dom';

import { Highlighted } from './../../ui/Home/Home'
import { 
  JumbotronLogoContainer, Text,
  ButtonsContainer, Button 
} from './../../ui/JumbotronLogo/JumbotronLogo';

class JumbotronLogo extends React.PureComponent {
  render() {
    return (
      <JumbotronLogoContainer>
        <Text>¿TE APETECE UN BUEN <Highlighted>CAFÉ</Highlighted>?</Text>
        <ButtonsContainer>
          <Link to="/order/select-shop"><Button>PÍDELO</Button></Link>
        </ButtonsContainer>
      </JumbotronLogoContainer>
    );
  }
}

export default JumbotronLogo;
