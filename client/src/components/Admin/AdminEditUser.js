import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// components
import { EditUserContainer, Button, UserCurrentCredit, CancelButton } from './../../ui/Admin/AdminEditUser';

import { editUser } from './../../redux/modules/admin/editUser';

class AdminEditUser extends React.PureComponent {
  static propTypes = {
    user: PropTypes.object.isRequired
  };

  state = {
    updatedCredit: Number(Math.round(this.props.user.currentCredit+'e2')+'e-2'),
    currentCredit: Number(Math.round(this.props.user.currentCredit+'e2')+'e-2'),
    isEditing: false,
    buttonText: "Editar"
  }

  onClickEditCurrentCredit = () => {
    if (this.state.isEditing) {
      this.submitEditUser()
    } else {
      this.setState({ isEditing: true, buttonText: "Guardar" })
    }
  }

  onCurrentCreditChange = (event) => {
    this.setState({ updatedCredit: event.target.value });
  }

  onCancelCurrentCreditChange = () => {
    this.setState({ isEditing: false, buttonText: "Editar", updatedCredit: this.state.currentCredit })
  }

  submitEditUser = () => {
    const { updatedCredit } = this.state
    const { email } = this.props.user
    this.props.dispatch(editUser({ email, currentCredit: updatedCredit  }));
    this.setState({ isEditing: false, buttonText: "Editar", currentCredit: updatedCredit })
  }

  render() {
    return (
      <EditUserContainer>
        <UserCurrentCredit type="number" value={this.state.updatedCredit} disabled={!this.state.isEditing} onChange={this.onCurrentCreditChange} />
        <Button onClick={this.onClickEditCurrentCredit}>{this.state.buttonText}</Button>
        <CancelButton style={{fontSize: '14px'}} onClick={this.onCancelCurrentCreditChange}>x</CancelButton>
      </EditUserContainer>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    EditUser: state.EditUser,
  };
};

export default connect(mapStateToProps)(AdminEditUser);
