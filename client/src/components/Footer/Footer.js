import React from 'react';
import PropTypes from 'prop-types'
import _ from 'lodash';
import { Link } from 'react-router-dom'

// components
import { FooterContainer, Logo, LogoContainer, FooterShops, FooterCoffeeShopTitle,
         FooterCoffeeShopsContainer, SocialMedia, FooterCoffeeShop } from './../../ui/Footer/Footer'

// images
import Icon from './img/barissta-logo.png'

class Footer extends React.PureComponent {
  static propTypes = {
    shops: PropTypes.array.isRequired,
  }

  renderFooterCoffeeShops = () => {
    const shops = this.props.shops;
    if (!_.isEmpty(shops)) {
      const shopsByCity = _.groupBy(shops, 'city')
      return (
        <FooterCoffeeShopsContainer>
          { Object.keys(shopsByCity).sort().map((city, c) => {
              const shopKey = `shops-${c}`
              return (
                <div key={c}>
                  <FooterCoffeeShopTitle key={c}>Cafeterías {city}</FooterCoffeeShopTitle>
                  <FooterShops key={shopKey}>
                    { shopsByCity[city].map((shop, i) => {
                      const url = `/coffee-shop/${shop.slug}`;
                      return (<FooterCoffeeShop key={i}><Link to={url}>{shop.name}</Link></FooterCoffeeShop>)
                    })}
                  </FooterShops>
                </div>
              )
            })
          }
        </FooterCoffeeShopsContainer>
      )
    }
  }

  render() {
    return (
      <FooterContainer>
        <LogoContainer>
          <Logo src={Icon} />
        </LogoContainer>
        <SocialMedia>
          <a href="https://www.facebook.com/IamBarissta/" target="_blank">
            <i className="fa fa-facebook fa-2x" style={{ color:"#000000", fontSize:"22px" }} aria-hidden="true"></i>
          </a>
          <a href="https://www.instagram.com/iam_barissta/" target="_blank">
            <i className="fa fa-instagram fa-2x" style={{ color:"#000000", fontSize:"25px" }} aria-hidden="true"></i>
          </a>
        </SocialMedia>
        {this.renderFooterCoffeeShops()}
      </FooterContainer>
    );
  }
}

export default Footer;
