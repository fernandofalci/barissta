// npm packages
import { takeLatest, put, call } from 'redux-saga/effects'

// utils
import RestClient from './../../../utils/rest-client'

// actions names
const EDIT_USER = 'ADMIN/EDIT_USER';
const EDIT_USER_FAILED = 'ADMIN/EDIT_USER_FAILED';
const EDIT_USER_SUCCESS = 'ADMIN/EDIT_USER_SUCCESS';

const initialState = {
  EditUser: {
    state: 'READY',
    error: '',
  },
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case EDIT_USER:
      return {
        ...state,
        EditUser: {
          ...state.EditUser,
          state: 'PENDING',
          error: '',
        },
      };
    case EDIT_USER_FAILED:
      return {
        ...state,
        EditUser: {
          ...state.EditUser,
          state: 'ERROR',
          error: action.error,
        },
      };
    case EDIT_USER_SUCCESS:
      return {
        ...state,
        EditUser: {
          ...state.EditUser,
          state: 'READY',
          error: '',
        },
      };
    default:
      return state;
  }
}

// action creators
export const editUser = (options) => {
  return {
    type: EDIT_USER,
    options  
  }
};

const editUserFailed = (error) => ({
  type: EDIT_USER_FAILED,
  error
});

const editUserSuccess = () => ({
  type: EDIT_USER_SUCCESS
});

// worker saga
function* editUserSaga(action) {
  try {
    const { email, currentCredit } = action.options;
    const response = yield call(RestClient.post, 'api/admin/edit/user', { email, currentCredit });
    const jsonResponse = yield response.json();
    yield put(editUserSuccess());
  } catch (e) {
    alert('Error editing user');
    console.error('Error editin user: ', e);
    return yield put(editUserFailed(`Error getting users: ${e}`));
  }
}

// watcher Saga
export function* adminEditUserSaga() {
  yield takeLatest(EDIT_USER, editUserSaga);
}
