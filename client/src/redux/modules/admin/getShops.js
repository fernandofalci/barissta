// npm packages
import { takeLatest, put, call } from 'redux-saga/effects'

// utils
import RestClient from './../../../utils/rest-client'

// actions names
const FETCH_SHOPS = 'ADMIN/FETCH_SHOPS';
const FETCH_SHOPS_FAILED = 'ADMIN/FETCH_SHOPS_FAILED';
const FETCH_SHOPS_SUCCESS = 'ADMIN/FETCH_SHOPS_SUCCESS';

const initialState = {
  shops: {
    state: 'READY',
    data: [],
    error: ''
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_SHOPS:
      return { ...state, shops: { ...state.Shops, state: 'PENDING', error: '', data: [] } };
    case FETCH_SHOPS_FAILED:
      return { ...state, shops: { ...state.Shops, state: 'ERROR', error: action.error, data: [] } };
    case FETCH_SHOPS_SUCCESS:
      return { ...state, shops: { ...state.Shops, state: 'READY', error: '', data: [...action.shops] } };
    default:
      return state;
  }
}

// action creators
export const fetchShops = () => ({
  type: FETCH_SHOPS
});

const fetchShopsFailed = (error) => ({
  type: FETCH_SHOPS_FAILED,
  error
});

const fetchShopsSuccess = (shops) => {
  return {
    type: FETCH_SHOPS_SUCCESS,
    shops    
  }
};

// worker Saga
function* fetchShopsSaga(action) {
  try {
    const response = yield call(RestClient.get, 'api/admin/shops/');
    const shops = yield response.json();
    yield put(fetchShopsSuccess(shops));
  } catch (e) {
    alert('Error getting shops');
    console.log('Error getting shops: ', e);
    return yield put(fetchShopsFailed(`Error getting shops: ${e}`));
  }
}

// watcher Saga
export function* adminShopsSaga() {
  yield takeLatest(FETCH_SHOPS, fetchShopsSaga);
}
