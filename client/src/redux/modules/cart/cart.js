import _ from 'lodash'

const INIT_CART = 'CART/INIT'
const ADD_TO_CART = 'CART/ADD';
const REMOVE_FROM_CART = 'CART/REMOVE';

const initialState = {
  items: {},
  shop: ''
};

export default function reducer(state = initialState, action) {
  let itemCurrentAmount
  switch (action.type) {
    case INIT_CART:
      return { items: {}, shop: action.shop }
    case ADD_TO_CART:
      const item = action.item
      itemCurrentAmount = _.has(state.items, [item.id]) ? state.items[item.id].amount : 0
      return { ...state, items: { ...state.items, [item.id]: { ...item, amount: itemCurrentAmount + 1 } } }
    case REMOVE_FROM_CART:
      const itemId = action.itemId
      itemCurrentAmount = _.has(state.items, [itemId]) ? state.items[itemId].amount : 0
      if (itemCurrentAmount > 1) {
        return { ...state, items: { ...state.items, [itemId]: { ...state.items[itemId], amount: itemCurrentAmount - 1 } } }
      }
      return { ...state, items: { ..._.omit(state.items, itemId) } }
    default:
      return state;
  }
}

// action creators
export const initCart = (shop) => ({
  type: INIT_CART,
  shop
})

export const addItemToCart = (item) => ({
  type: ADD_TO_CART,
  item
})

export const removeItemFromCart = (itemId) => ({
  type: REMOVE_FROM_CART,
  itemId
})
