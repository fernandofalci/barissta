import { takeLatest, put, call } from 'redux-saga/effects'
import { setUser } from '../user/set'
import { initCart } from '../cart/cart'

// utils
import RestClient from './../../../utils/rest-client'

// actions names
const PAYING = 'PAYMENT/PAYING';
const PAY_SUCCESS = 'PAYMENT/PAY_SUCCESS';
const PAY_FAILED = 'PAYMENT/PAY_FAILED';

const initialState = {
  payment: {
    state: 'READY',
    data: {},
    error: ''
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case PAYING:
      return { ...state, payment: { ...state.payment, state: 'PENDING', error: '', data: {} } };
    case PAY_FAILED:
      return { ...state, payment: { ...state.payment, state: 'ERROR', error: action.error, data: {} } };
    case PAY_SUCCESS:
      return { ...state, payment: { ...state.payment, state: 'READY', error: '', data: action.payment } };
    default:
      return state;
  }
}

// action creators
export const doPay = (options) => {
  return {
    type: PAYING,
    options  
  }
};

const paymentFailed = (error) => ({
  type: PAY_FAILED,
  error
});

const paymentSuccess = (payment) => ({
  type: PAY_SUCCESS,
  payment
});

// worker Saga
function* handlePaymentSaga(action) {
  try {
    const { cart } = action.options;
    const response = yield call(RestClient.post, 'api/payment/pay', { cart });

    if (response.status >= 200 && response.status < 300) {
      const token = yield response.json();
      yield put(setUser(token))
      yield put(initCart(cart.shop))
      yield put(paymentSuccess(token))
    } else {
      throw response;
    }
  } catch (e) {
    alert('No se ha podido realizar el pago. Comprueba que tu tarjeta es válida.');
    console.error('Error with payment: ', e);
    return yield put(paymentFailed(`Error with payment: ${e}`));
  }
}

// watcher Saga
export function* paymentSaga() {
  yield takeLatest(PAYING, handlePaymentSaga);
}
