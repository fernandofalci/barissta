// npm packages
import { put, takeLatest } from 'redux-saga/effects';

// utils
import { deleteTokenCookie } from './../../../utils/manageCookies';
import { deleteLocalUser } from './../user/set';

// actions names
const LOGOUT = 'AUTH_LOGOUT/LOGOUT';

const initialState = {};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return state;
    default:
      return state;
  }
}

export const logoutUser = () => ({
  type: LOGOUT,
});

// worker Saga
function* logoutUserSaga(action) {
  try {
    yield deleteTokenCookie();
    yield put(deleteLocalUser());
  } catch (e) {
    console.error('Error loging out a user: ', e);
  }
}

// watcher Saga
export function* authLogoutSaga() {
  yield takeLatest(LOGOUT, logoutUserSaga);
}
