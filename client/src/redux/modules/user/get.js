// npm packages
import { takeLatest, put, call } from 'redux-saga/effects'

// utils
import { getTokenCookie, setTokenCookie } from './../../../utils/manageCookies'
import RestClient from './../../../utils/rest-client'

// actions
import { setUser } from './set'

// actions names
const GET_LOCAL_USER = 'USER_GET/GET_LOCAL_USER';
const GET_UPDATED_TOKEN = 'USER_GET/GET_UPDATED_TOKEN';
const GET_UPDATED_TOKEN_FAILED = 'USER_GET/GET_UPDATED_TOKEN_FAILED';
const GET_UPDATED_TOKEN_SUCCESS = 'USER_GET/GET_UPDATED_TOKEN_SUCCESS';

const initialState = {
  isRequesting: false,
  requestSuccess: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_LOCAL_USER:
      return state;
    case GET_UPDATED_TOKEN:
      return { ...state, isRequesting: true, requestSuccess: false };
    case GET_UPDATED_TOKEN_FAILED:
      return { ...state, isRequesting: false, requestSuccess: false };
    case GET_UPDATED_TOKEN_SUCCESS:
      return { ...state, isRequesting: false, requestSuccess: true };
    default:
      return state;
  }
}

// action creators
export const getLocalUser = () => ({
  type: GET_LOCAL_USER,
});

export const getUpdatedToken = () => ({
  type: GET_UPDATED_TOKEN,
});

const getUpdatedTokenFailed = () => ({
  type: GET_UPDATED_TOKEN_FAILED,
});

const getUpdatedTokenSuccess = () => ({
  type: GET_UPDATED_TOKEN_SUCCESS,
});

// worker Saga
function* getLocalUserSaga() {
  try {
    const token = getTokenCookie();
    if (token) {
      return yield put(getUpdatedToken());
    }
  } catch (e) {
    console.error('An error occurred when trying to read a cookie: ', e);
  }
}

function* getUpdatedTokenSaga(action) {
  try {
    const response = yield call(RestClient.get, 'api/user/update-token');
    if (response.headers && response.headers.get('content-type').indexOf('application/json') >= 0) {
      const updatedToken = yield response.json();
      if (response.status >= 400) {
        console.error('Error login: ', updatedToken);
        return yield put(getUpdatedTokenFailed());
      } else {
        setTokenCookie(updatedToken);
        yield put(setUser(updatedToken));
        return yield put(getUpdatedTokenSuccess());
      }
    }
    console.error('Error on login request: ', response.statusText);
    return yield put(getUpdatedTokenFailed());
  } catch (e) {
    console.error('Error login a user: ', e);
  }
}

// watcher Saga
export function* userGetSaga() {
  yield takeLatest(GET_LOCAL_USER, getLocalUserSaga);
  yield takeLatest(GET_UPDATED_TOKEN, getUpdatedTokenSaga);
}
