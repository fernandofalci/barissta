import jwtDecode from 'jwt-decode';

// actions names
const SET_USER = 'USER_SET/SET_USER';
const SET_USER_COFFEES = 'USER_SET/SET_USER_COFFEES';
const DELETE_LOCAL_USER = 'USER_SET/DELETE_LOCAL_USER';

const initialState = {
  User: {
    state: 'READY',
    data: {},
    error: '',
  },
};

export default function reducer(state = initialState, action) {
  let newUser;
  switch (action.type) {
    case SET_USER:
      newUser = jwtDecode(action.token);
      return {
        ...state,
        User: {
          ...state.User,
          data: newUser,
          error: '',
          state: 'READY',
        },
      };
    case DELETE_LOCAL_USER:
      return {
        ...state,
        User: {
          ...state.User,
          data: {},
          error: '',
          state: 'READY',
        },
      };
    case SET_USER_COFFEES:
      newUser = {
        User: {
          ...state.User,
          data: { ...state.User.data, remainingCoffees: action.remainingCoffees },
          error: '',
          state: 'READY',
        },
      };
      return {
        ...state,
        User: {
          ...state.User,
          data: newUser,
          error: '',
          state: 'READY',
        },
      };
    default:
      return state;
  }
}

// action creators
export const setUser = token => ({
  type: SET_USER,
  token,
});

export const setUserCoffees = remainingCoffees => ({
  type: SET_USER_COFFEES,
  remainingCoffees,
});

export const deleteLocalUser = () => ({
  type: DELETE_LOCAL_USER,
});
