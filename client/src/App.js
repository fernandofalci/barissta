import React from 'react'
import { ThemeProvider } from 'styled-components'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Sidebar } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'

import { connect } from 'react-redux'
import io from 'socket.io-client'

// components
import { AppContainer } from './ui/App/App'
import ScrollToTop from './components/ScrollToTop/ScrollToTop'
import Header from './components/Header/Header'
import SidebarComponent from './components/Sidebar/Sidebar'
import HomeContainer from './containers/HomeContainer/HomeContainer'
import LoginContainer from './containers/LoginContainer/LoginContainer'
import SignupContainer from './containers/SignupContainer/SignupContainer'
import ProfileContainer from './containers/ProfileContainer/ProfileContainer'
// import InviteContainer from './containers/InviteContainer/InviteContainer'
import ChangePasswordContainer from './containers/ChangePasswordContainer/ChangePasswordContainer'
import AddCoffeeShop from './containers/AddCoffeeShopContainer/AddCoffeeShopContainer'
import CoffeeShop from './containers/CoffeeShopContainer/CoffeeShopContainer'
import RecoverPassword from './components/RecoverPassword/RecoverPassword'
import EmailSignupContainer from './containers/SignupContainer/EmailSignupContainer'
import OrderSelectShopContainer from './containers/OrderContainer/OrderSelectShopContainer'
import OrderPaymentMethodContainer from './containers/OrderContainer/OrderPaymentMethodContainer'
import OrderCheckoutContainer from './containers/OrderContainer/OrderCheckoutContainer'

import AdminContainer from './containers/AdminContainer/AdminContainer'

// theme
import { mainTheme } from './ui/themes/themes'

// styles
import { Main, CookieWarningBlock, CookieWarningButton, CookieWarningText, SidebarShadow } from './ui/Main/Main';

// actions
import { getLocalUser } from './redux/modules/user/get'
import { setUser } from './redux/modules/user/set'

// config
import { apiHost } from './utils/config';

// utils
import { getCookieWarningCookie, setCookieWarningCookie } from './utils/manageCookies';

class App extends React.PureComponent {
  state = {
    sidebarOpen: false,
    showCookieWarning: false
  };

  componentWillMount() {
    const cwc = getCookieWarningCookie()
    if (!cwc) {
      this.setState({ showCookieWarning: true })
      setCookieWarningCookie()
    }
  }

  componentDidMount() {
    const { getLocalUser } = this.props;
    getLocalUser();

    this.socket = io(apiHost);
  }

  toggleSidebar = () => {
    this.setState(prevState => ({ sidebarOpen: !prevState.sidebarOpen }))
  };

  hideSidebar = () => {
    this.setState({ sidebarOpen: false })
  }

  onClickAcceptCookies = () => {
    this.setState({ showCookieWarning: false })
  }

  getAppContent() {
    const { User } = this.props
    const { showCookieWarning, sidebarOpen } = this.state
    const sidebarClass = sidebarOpen ? 'sidebarOpen' : ''
    this.socket &&
      User.data &&
      this.socket.on(`${User.data._id}`, token => {
        this.props.setUser(token.token);
      });
    return (
      <AppContainer>
        <Header toggleSidebar={this.toggleSidebar} hideSidebar={this.hideSidebar} User={User.data} />
        <SidebarComponent User={User.data} sidebarOpen={sidebarOpen} hideSidebar={this.hideSidebar} />
        <SidebarShadow onClick={this.hideSidebar} className={sidebarClass}></SidebarShadow>
        <Sidebar.Pusher as={Main}>
          <ScrollToTop>
            <Switch>
              <Route exact path="/" component={HomeContainer} />
              <Route path="/admin" component={AdminContainer} />
              <Route path="/login" component={LoginContainer} />
              <Route exact path="/signup" component={SignupContainer} />
              <Route path="/signup/email" component={EmailSignupContainer} />
              <Route path="/profile" component={ProfileContainer} />
              {/*<Route path="/invite" component={InviteContainer} />*/}
              <Route path="/cambiar-password" component={ChangePasswordContainer} />
              <Route path="/recover-password/:token" component={RecoverPassword} />
              <Route path="/recover-password" component={RecoverPassword} />
              <Route path="/add-coffee-shop" component={AddCoffeeShop} />
              <Route exact path="/coffee-shop/:slug" component={CoffeeShop} />
              <Route exact path={"/order/select-shop"} component={OrderSelectShopContainer} />
              <Route exact path={"/order/payment"} component={OrderPaymentMethodContainer} />
              <Route exact path={"/order/checkout"} component={OrderCheckoutContainer} />
              <Route component={HomeContainer} />
            </Switch>
          </ScrollToTop>
        </Sidebar.Pusher>
        {showCookieWarning && (
          <CookieWarningBlock>
            <CookieWarningText>Si sigues navegando aceptas el uso de cookies, perfectas para acompañar tu café 🍪 ☕</CookieWarningText>
            <CookieWarningButton onClick={this.onClickAcceptCookies}>Aceptar</CookieWarningButton>
            <CookieWarningButton alt>
              <a href='https://docs.google.com/document/d/1VoKUcKQsCLU8qb8Bph9fB3onj0IyevBAE5wkA-yV5WE/pub' target='_blank'>
                Más información
              </a>
            </CookieWarningButton>
          </CookieWarningBlock>
        )}
      </AppContainer>
    )
  }

  render() {
    return (
      <BrowserRouter>
        <ThemeProvider theme={mainTheme}>
          {this.getAppContent()}
        </ThemeProvider>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = ({ userSet }, ownProps) => {
  return {
    User: userSet.User,
  }
}

App = connect(mapStateToProps, {
  getLocalUser,
  setUser
})(App)

export default App
