import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

import Login from './../../components/Login/Login'

import { login } from './../../redux/modules/auth/login'

class LoginContainer extends React.PureComponent {
  render() {
    const {loginState, login, User} = this.props;
    if (Object.keys(User.data).length > 0) {
      return <Redirect to='/profile' />
    }
    return (
      <Login onLogin={login} loginState={loginState} />
    )
  }
}

const mapStateToProps = ({ userSet, authLogin }) => {
  return {
    User: userSet.User,
    loginState: authLogin
  }
}

LoginContainer = connect(mapStateToProps, {
  login
})(LoginContainer)

export default LoginContainer
