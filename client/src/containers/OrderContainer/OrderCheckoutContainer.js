import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import _ from 'lodash'

import OrderCheckout from './../../components/Order/OrderCheckout'

// actions
import { getShop } from './../../redux/modules/shop/getShop'
import { removeItemFromCart } from './../../redux/modules/cart/cart'
import { doPay } from './../../redux/modules/payment/actions'

class OrderCheckoutContainer extends React.PureComponent {
  state = {
    paymentSucceeded: false
  }

  componentDidMount() {
    const shopData = this.props.shop.data
    const { getShop, match } = this.props
    if (_.isEmpty(shopData)) {
      getShop(match.params.slug);
    }
  }

  removeItem = (itemId) => {
    this.props.removeItemFromCart(itemId)
  }

  componentWillReceiveProps(nextProps) {
    const nextPaymentState = nextProps.paymentState
    const { paymentState } = this.props
    if (paymentState === 'PENDING' && nextPaymentState === 'READY') {
      this.setState({ paymentSucceeded: true })
    }
  }

  render() {
    const shopData = this.props.shop.data
    const { match, cart, user, doPay, paymentState } = this.props
    const { paymentSucceeded } = this.state

    if (paymentSucceeded) {
      return <Redirect to={{ pathname: '/profile', state: { orderSucceeded: true }}} />
    }

    if (!shopData.items) {
      return <Redirect to={`/coffee-shop/${match.params.slug}`} />
    }
    return (
      <OrderCheckout 
        user={user.data}
        cart={cart}
        shop={shopData}
        removeItem={this.removeItem}
        doPay={doPay}
        paymentState={paymentState}
      />
    )
  }
}

const mapStateToProps = ({ shopGet, cart, userSet, paymentActions }) => {
  return {
    shop: shopGet.Shop,
    cart,
    user: userSet.User,
    paymentState: paymentActions.payment.state
  }
}

OrderCheckoutContainer = connect(mapStateToProps, {
  getShop,
  removeItemFromCart,
  doPay
})(OrderCheckoutContainer)

export default OrderCheckoutContainer
