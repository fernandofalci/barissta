import _ from 'lodash'
import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

import Signup from './../../components/Signup/Signup'

import { login } from './../../redux/modules/auth/login'

class SignupContainer extends React.PureComponent {

  render() {
    const { User, loginState, login } = this.props
    if (Object.keys(User.data).length > 0) {
      return <Redirect to='/profile' />
    }
    return (
      <Signup onFacebookLogin={login} facebookLoginState={loginState} />
    )
  }
}

const mapStateToProps = ({ userSet, authLogin }) => {
  return {
    User: userSet.User,
    loginState: authLogin
  }
}

SignupContainer = connect(mapStateToProps, {
  login
})(SignupContainer)

export default SignupContainer
