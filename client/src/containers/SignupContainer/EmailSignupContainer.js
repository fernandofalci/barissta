import _ from 'lodash';
import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

import EmailSignup from './../../components/Signup/EmailSignup'

import { signup } from './../../redux/modules/auth/signup'

class EmailSignupContainer extends React.PureComponent {
  render() {
    const {signup, User, signupState} = this.props;
    if (Object.keys(User.data).length > 0) {
      return <Redirect to='/profile' />;
    }
    return (
      <EmailSignup onSignup={signup} signupState={signupState} />
    );
  }
}

const mapStateToProps = ({ userSet, authSignup }) => {
  return {
    User: userSet.User,
    signupState: authSignup
  };
};

EmailSignupContainer = connect(mapStateToProps, {
  signup
})(EmailSignupContainer);

export default EmailSignupContainer;
