import React from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'

// components
import Item from './../../components/Item/Item'

// actions
import { addItemToCart, removeItemFromCart } from './../../redux/modules/cart/cart'

class ItemContainer extends React.PureComponent {
  addItem = (item) => {
    this.props.addItemToCart(item)
  }

  removeItem = (item) => {
    this.props.removeItemFromCart(item)
  }

  getItemAmountInCart = () => {
    const { item, cart } = this.props
    const amount = _.get(cart, `items.${item.id}.amount`, 0);
    return amount
  }

  render() {
    const { item } = this.props
    return (
      <Item item={item} addItem={this.addItem} removeItem={this.removeItem} itemAmountInCart={this.getItemAmountInCart()}></Item>
    )
  }
}

const mapStateToProps = ({ cart }) => {
  return {
    cart
  }
}

ItemContainer = connect(mapStateToProps, {
  addItemToCart,
  removeItemFromCart
})(ItemContainer)

export default ItemContainer
