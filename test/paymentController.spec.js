import proxyquire from 'proxyquire'
import sinon from 'sinon'
import User from '../src/User/model/user'
import userService from '../src/User/userService'
const expect = require('chai').expect

const paymentServiceStub = {
  paymentServiceFactory() {
    return {
      chargePayment() {
        return Promise.resolve(true)
      }
    }
  }
}
const paymentController = proxyquire('./../src/payment/controller', { './paymentService': paymentServiceStub });

describe('Payment Controller', () => {
  // it('chargePlan returns 500 error when invalid Plan is provided', async () => {
  //   const req = {
  //     body: { plan: 'dummy' }
  //   }
  //   const res = {
  //     status() {
  //       return {
  //         json() {
  //           return true
  //         }
  //       } 
  //     }
  //   }
  //   const statusSpy = sinon.spy(res, 'status');
  //   const request = await paymentController.chargePlan(req, res)
  //   expect(statusSpy.calledOnce).to.be.true
  //   return expect(statusSpy.calledWith(500)).to.be.true
  // })

  // it('chargePlan adds coffee cups successfully', async () => {
  //   const customer = {
  //     id: 'cus'
  //   }
  //   const user = new User({ email: 'jose@example.com', password: '123', customer })
  //   await user.save()
  //   const req = {
  //     body: { plan: 'social' },
  //     token: { _id: user._id }
  //   }
  //   const res = {
  //     status() {
  //       return {
  //         json(e) {
  //           return e
  //         }
  //       }
  //     }
  //   }
  //   await paymentController.chargePlan(req, res)
  //   const updatedUser = await userService.getUserById(user._id)
  //   return expect(updatedUser.remainingCoffees).to.equal(6)
  // })
})