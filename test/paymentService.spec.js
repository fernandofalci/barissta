import sinon from 'sinon'
const expect = require('chai').expect

const PaymentService = require('./../src/payment/paymentService')
import User from './../src/User/model/user'

describe('Payment Service', () => {

  let paymentService

  before(() => {
    const customers = {
      create({ tokenId, email }) {
        return Promise.resolve({
          id: 'cus_AnzienEVMGCBRC',
          object: 'customer',
          account_balance: 0,
          currency: 'eur',
          description: null,
          email,
          sources: {
            data: [
              {
                id: 'card_1AUdUiAf5WTAs1pY0331hCi7',
                object: 'card',
                country: 'US',
                brand: 'Visa',
                customer: 'cus_AnzienEVMGCBRC',
                exp_month: 12,
                exp_year: 2020,
                last4: '4242'
              }
            ]
          }
        })
      }
    }
    const charges = {
      create({ amount, customer, source }) {
        return Promise.resolve({
          id: 'ch_1AUB3jAf5WTAs1pYBTHgRSZE',
          object: 'charge',
          amount,
          captured: false,
          currency: 'eur',
          customer,
          paid: true,
          source: source ? {
            id: 'card_1AUB3iAf5WTAs1pYoWqzZYao',
            object: 'card'
          } : null,
          status: 'succeeded'
        })
      }
    }
    const stripe = {
      customers,
      charges
    }
    paymentService = PaymentService.paymentServiceFactory(stripe)
  });


  it('createCustomer should return customer data', async () => {
    const email = 'test@example.com'
    const tokenId = 'tok_visa_debit'
    const user = new User({ email, password: '1234' })

    const customer = await paymentService.createCustomer(user, tokenId)
    expect(customer).to.have.property('id').that.is.a('string')
    expect(customer).to.have.property('cards').that.is.a('array')
    expect(customer.cards).to.have.lengthOf(1)
    return expect(customer.cards[0]).to.have.property('last4').that.is.a('string')
  })

  it('chargePayment should successfully charge payment from card token', async () => {
    const amount = 1000
    const options = {
      source: 'tok_visa_debit'
    }
    const charge = await paymentService.chargePayment(amount, options)
    expect(charge).to.have.property('source').that.is.a('object')
    expect(charge.status).to.equal('succeeded')
    expect(charge.paid).to.equal(true)
    return expect(charge.amount).to.equal(amount)
  })

  it('chargePayment should successfully charge payment from customer', async () => {
    const amount = 1000
    const options = {
      customer: 'cus_AnzienEVMGCBRC'
    }
    const charge = await paymentService.chargePayment(amount, options)
    expect(charge.customer).to.equal(options.customer)
    expect(charge.status).to.equal('succeeded')
    expect(charge.paid).to.equal(true)
    return expect(charge.amount).to.equal(amount)
  })
})
