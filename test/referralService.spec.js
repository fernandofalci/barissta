import mongoose from 'mongoose'
const expect = require('chai').expect
import referralService from './../src/Referral/referralService'

describe('Referral Service', () => {
  it(`createPromoCode should successfully create promo code for provided User`, async () => {
    const randomUserId = mongoose.Types.ObjectId()
    const promoCode = await referralService.createPromoCode(randomUserId)
    expect(promoCode).to.have.property('code').that.is.a('string')
    return expect(promoCode.referredUsers).to.be.an('array').that.is.empty;
  })

  it(`createPromoCode should fail creating a second promo code for provided User`, async () => {
    const randomUserId = mongoose.Types.ObjectId()
    const promoCode = await referralService.createPromoCode(randomUserId)
    expect(promoCode).to.have.property('code').that.is.a('string')
    const anotherPromoCode = await referralService.createPromoCode(randomUserId)
    expect(anotherPromoCode).to.be.null
  })

  it('isPromoCodeValid should return true if provided code exists', async () => {
    const randomUserId = mongoose.Types.ObjectId()
    const promoCode = await referralService.createPromoCode(randomUserId)
    const isPromoCodeValid = await referralService.isPromoCodeValid(promoCode.code)
    return expect(isPromoCodeValid).to.be.true
  })

  it(`isPromoCodeValid should return false if provided code doesn't exist`, async () => {
    const randomCode = 'fakkeeeCode'
    const isPromoCodeValid = await referralService.isPromoCodeValid(randomCode)
    return expect(isPromoCodeValid).to.be.false
  })

  it(`getUserPromoCode should return promo code generated for provided User`, async () => {
    const randomUserId = mongoose.Types.ObjectId()
    const promoCode = await referralService.createPromoCode(randomUserId)
    const retrievedPromoCode = await referralService.getUserPromoCode(randomUserId)
    return expect(retrievedPromoCode._id.toString()).to.equal(promoCode._id.toString())
  })

  it(`getUserPromoCode should return null if provided User doesn't have a promo code`, async () => {
    const randomUserId = mongoose.Types.ObjectId()
    const retrievedPromoCode = await referralService.getUserPromoCode(randomUserId)
    return expect(retrievedPromoCode).to.be.null
  })
})
