// npm packages
import assert from 'assert'

// models
import User from './../src/User/model'

describe('Reading users out of the database', () => {
  let joe;

  beforeEach((done) => {
    joe = new User({ firstName: 'joe', email: 'joe@gmail.com', password: 'pass1234' });
    joe.save()
      .then(() => done())
      .catch(error => console.error('Error reading: ', error));
  });

  it('finds all users with a name of joe', (done) => {
    User.find({ firstName: 'joe' })
      .then(users => {
        assert(users[0]._id.toString() === joe._id.toString());
        done();
      })
      .catch(error => {
        console.log(`Error reading joe: `, error);
        done();
      });
  });

  it('find a user with a particular id', (done) => {
    User.findOne({ _id: joe._id })
      .then(user => {
        assert(user.firstName === joe.firstName);
        done();
      })
      .catch(error => {
        console.error('Error findOne by id: ', error);
        done();
      })
  });
})