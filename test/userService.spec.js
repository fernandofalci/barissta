import mongoose from 'mongoose'
const expect = require('chai').expect
import User from './../src/User/model/user'
import userService from './../src/User/userService'

describe('User Service', () => {
  it('getUserById should successfully get User for valid Id', async () => {
    const user = new User({ email: 'test@example.com', password: '1234' })
    const userId = user._id
    await user.save()
    const retrievedUser = await userService.getUserById(userId)
    return expect(userId.toString()).to.equal(retrievedUser._id.toString())
  })

  it('getUserById should return null when No User matches provided Id', async () => {
    const anyId = mongoose.Types.ObjectId();
    const retrievedUser = await userService.getUserById(anyId)
    return expect(retrievedUser).to.be.null
  })

  it('getUserByEmail should successfully get User for valid email', async () => {
    const email = 'test@example.com'
    const user = new User({ email, password: '1234' })
    const userId = user._id
    await user.save()
    const retrievedUser = await userService.getUserByEmail(email)
    return expect(userId.toString()).to.equal(retrievedUser._id.toString())
  })

  it('getUserByEmail should return null when No User matches provided email', async () => {
    const email = 'test@example.com'
    const retrievedUser = await userService.getUserByEmail(email)
    return expect(retrievedUser).to.be.null
  })

  it('addCoffeesToUser should successfully add specified number of cups to User', async () => {
    const user = new User({ email: 'test@example.com', password: '1234' })
    await user.save()
    expect(user.remainingCoffees).to.equal(undefined)
    const cupsToAdd = 10
    const updatedUser = await userService.addCoffeesToUser(user._id, cupsToAdd)
    return expect(updatedUser.remainingCoffees).to.equal(cupsToAdd)
  })

  it('addCoffeesToUser should return null when provided User Id does not exist', async () => {
    const cupsToAdd = 10
    const fakeUserId = '59075af64d8aebea3e5354eb'
    const updatedUser = await userService.addCoffeesToUser(fakeUserId, cupsToAdd)
    return expect(updatedUser).to.be.null
  })

  it('saveCustomer should successfully save customer data for User', async () => {
    const user = new User({ email: 'test@example.com', password: '1234' })
    await user.save()
    expect(user.toObject()).to.not.have.property('customer')
    const customerData = {
      id: 'cuscus',
      cards: [{
        brand: 'Visa',
        last4: '4242',
        id: 'cardddd',
        exp_month: 12,
        exp_year: 2025
      }]
    } 
    const updatedUser = await userService.saveCustomer(user._id, customerData)
    const updatedUserCard = updatedUser.customer.cards[0]
    expect(updatedUserCard.id).to.equal(customerData.cards[0].id)
    return expect(updatedUser.customer.id).to.equal(customerData.id)
  })

  it('saveCustomer should return null when provided User Id does not exist', async () => {
    const fakeUserId = mongoose.Types.ObjectId()
    const customerData = {
      id: 'cuscus'
    }
    const updatedUser = await userService.saveCustomer(fakeUserId, customerData)
    return expect(updatedUser).to.be.null
  })
})
