import joi from 'joi'

const areParamsValid = (params, schema) => {
  const { error } = joi.validate(params, schema)
  return error === null
}

export {
  areParamsValid
}
