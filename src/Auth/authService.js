import fs from 'fs'
import jwt from 'jsonwebtoken'
import _ from 'underscore'

import UserToken from '../models/UserToken'
import userService from '../User/userService'

const updateUserToken = async userId => {
  const user = await userService.getUserById(userId)
  if (_.isEmpty(user)) {
    return null
  }
  const cert = fs.readFileSync('private.key').toString()
  const userTokenProperties = _.pick(user, '_id',
                                           'email',
                                           'name',
                                           'isAdmin',
                                           'currentCredit',
                                           'futureCredit')

  if (!_.isUndefined(user.customer) && !_.isEmpty(user.customer.cards)) {
    userTokenProperties['card'] = _.pick(user.customer.cards[0], 'brand', 'last4', 'exp_month', 'exp_year')
  }
  const token = jwt.sign(userTokenProperties, cert, { expiresIn: '7d' })
  return token
}

const getAuthProviderToken = (id, provider) => {
  return UserToken.findOne({ id, provider })
}

const saveAuthProviderToken = ({ userId, provider, id }) => {
  const userToken = new UserToken({ userId, provider, id })
  userToken.save()
}

export default {
  updateUserToken,
  getAuthProviderToken,
  saveAuthProviderToken
}
