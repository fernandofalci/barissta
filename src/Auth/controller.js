import passwordHash from 'password-hash'
import joi from 'joi'
import _ from 'underscore'

import userService from '../User/userService'
import emailService from '../Email/emailService'
import creditService from '../Credit/creditService'
import authService from './authService'

import { getDisplayName } from './../User/util'
import { areParamsValid } from './util'

const emailLoginSchema = joi.object().keys({
  email: joi.string().email().required(),
  password: joi.string().required()
})

const signupSchema = joi.object().keys({
  email: joi.string().email().required(),
  name: joi.string().required(),
  password: joi.string().required()
})

const login = async (req, res) => {
  const { email, password } = req.body
  if (!areParamsValid({ email, password }, emailLoginSchema)) {
    return res.status(400).json('Los datos introducidos no son válidos')
  }

  const parsedEmail = email.toLowerCase()
  const user = await userService.getUserByEmail(parsedEmail)
  if (!user || !passwordHash.verify(password, user.password)) {
    return res.status(400).json('Email o contraseña incorrectos')
  }

  await creditService.activateLandingCreditToUser(user)

  const token = await authService.updateUserToken(user._id)
  return res.status(200).json({ token, isNewUser: false })
}

const loginWithToken = async (req, res) => {
  if (!req.user) {
    return res.send(401, 'Usuario no autenticado')
  }

  const updatedUser = await creditService.activateLandingCreditToUser(req.user)

  const isNewUser = req.user.isNew
  const tokenUser = _.omit(updatedUser, 'isNew')
  const token = await authService.updateUserToken(tokenUser._id)
  return res.status(200).json({ token, isNewUser })
}

const signup = async (req, res) => {
  const { name, email, password } = req.body
  if (!areParamsValid({ name, email, password }, signupSchema)) {
    return res.status(400).json('Los datos introducidos no son válidos')
  }

  const parsedEmail = email.toLowerCase()
  const user = await userService.getUserByEmail(parsedEmail)
  if (user) {
    return res.status(400).json('Email ya registrado')
  }

  const hashedPassword = passwordHash.generate(password)
  const newUser = await userService.createUser({ name, email, password: hashedPassword })

  await creditService.activateLandingCreditToUser(newUser)

  const displayName = getDisplayName(newUser.name)
  emailService.sendEmail('NewUserEmail', newUser.email, 'Ya eres Barissta ☕', { name: displayName })

  const token = await authService.updateUserToken(newUser._id)
  res.status(200).json(token)
}

export {
  login,
  loginWithToken,
  signup
}
