import path from 'path'
import http from 'http'
import express from 'express'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import cors from 'cors'
import ioFunc from 'socket.io'
import compression from 'compression'
import { PORT, staticAssetsPath } from './config'
import creditScheduler from './Credit/creditScheduler'
import common from './routes'
import users from './User/routes'
import auth from './Auth/routes'
import shops from './Shop/routes'
import admin from './Admin/routes'
// import referral from './Referral/routes'
import payment from './Payment/routes'
import location from './Location/routes'

process.on('uncaughtException', err => {
  console.error(err)
  process.exit(1)
})

const app = express()
const server = http.Server(app)
const io = ioFunc().listen(server)

app.enable('trust proxy')

app.use(compression())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

// set mongoose mpromise to global promise
mongoose.Promise = global.Promise

// Connect mongo database
mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true })
  .then(() => console.log('mongoose connected!'))
  .catch(err => console.error('mongoose error connecting: ', err))

if (process.env.NODE_ENV === 'production') {
  app.use((req, res, next) => {
    const schema = req.get('X-Forwarded-Proto')
    if (schema !== 'https') {
      return res.redirect(`https://${req.hostname}${req.url}`)
    } else {
      next()
    }
  })
}

creditScheduler.initializeCreditScheduler()

// client assets
app.use(express.static(staticAssetsPath))

// email assets
app.use('/static-email', express.static(path.join(__dirname, '/../notifications/assets')))

app.use('/api/payment', payment)
app.use('/api/auth', auth)
app.use('/api/user', users)
app.use('/api/shop', shops)
app.use('/api/admin', admin)
// app.use('/api/referral', referral)
app.use('/api/location', location)
app.use('*', common)

app.use(function (err, req, res, next) {
  console.error('ERROR!!!! : ', err.stack)
  res.status(500).send('Something broke!')
})

io.on('connection', () => {
  console.log('connected!!')
})

server.listen(PORT, () => console.info(`Listening on port ${PORT}`))

export default app

export { io }
