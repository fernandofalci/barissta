import mongoose from 'mongoose'

const Schema = mongoose.Schema

const CustomerSchema = new Schema(
  {
    id: {
      type: String,
      required: true
    },
    cards: [
      {
        _id: false,
        id: {
          type: String,
          required: true
        },
        last4: {
          type: String,
          required: true
        },
        brand: {
          type: String,
          required: true
        },
        exp_month: {
          type: Number,
          required: true
        },
        exp_year: {
          type: Number,
          required: true
        },
        createdAt: {
          type: Date,
          default: Date.now
        }
      }
    ],
    createdAt: {
      type: Date,
      default: Date.now
    }
  },
  { _id: false }
)

export default CustomerSchema
