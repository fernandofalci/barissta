import mongoose from 'mongoose'
import ItemSchema from './schema/item'

const Item = mongoose.model('item', ItemSchema)

export default Item
