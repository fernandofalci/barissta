// npm packages
import express from 'express'
import multer from 'multer'

// controllers
import { getAllShops, getShopBySlug, setLocationCoords, addShop } from './controller'
import { isAdminMiddleware } from '../utils'

const storage = multer.memoryStorage()
const upload = multer({ storage })
const router = express.Router()

router.get('/', getAllShops)
router.get('/:slug', getShopBySlug)
router.get('/set-location-coords', setLocationCoords)
router.post('/', isAdminMiddleware, upload.single('image'), addShop)

export default router
