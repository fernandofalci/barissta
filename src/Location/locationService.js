import fetch from 'node-fetch'

const getSupportedRegions = () => {
  return [
    'Valencia',
    'Madrid'
  ]
}

const getLocationByIP = async (ip) => {
  const geolocationURL = `http://freegeoip.net/json/${ip}`
  const response = await fetch(geolocationURL)
  const location = await response.json()
  return location
}

export { getSupportedRegions, getLocationByIP }
