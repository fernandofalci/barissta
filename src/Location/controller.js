import _ from 'underscore'
import { getLocationByIP, getSupportedRegions } from './locationService'

const getCurrentCity = async (req, res) => {
  const defaultCity = 'madrid'
  const result = { city: defaultCity }

  const location = await getLocationByIP(req.ip)
  const supportedRegions = getSupportedRegions()
  const locationRegion = location.region_name
  if (locationRegion && _.contains(supportedRegions, locationRegion)) {
    result.city = locationRegion.toLowerCase()
  }
  return res.status(200).json(result)
}

export { getCurrentCity }
